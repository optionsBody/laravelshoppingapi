<?php
/**
 * Created by PhpStorm.
 * User: adamalyan
 * Date: 07/05/15
 * Time: 19:52
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Awesome\Customer;

class CustomerTableSeeder extends Seeder {

    public function run()
    {
        DB::table('customer')->delete();

        Customer::create([
            'email' => 'k.marx@mail.com',
            'first_name' => 'Karl',
            'last_name' => 'Marx',
            'address' => 'Cromwell Road, London, UK',
        ]);
        Customer::create([
            'email' => 'adam.smith@mail.com',
            'first_name' => 'Adam',
            'last_name' => 'Smith',
            'address' => 'Leith Walk, Edinburgh, UK',
        ]);
        Customer::create([
            'email' => 'g.Myrdal@mail.com',
            'first_name' => 'Gunnar',
            'last_name' => 'Myrdal',
            'address' => 'Tensta, Danderyd, Sweden',
        ]);
        Customer::create([
            'email' => 'm.friedman@mail.com',
            'first_name' => 'Milton',
            'last_name' => 'Friedman',
            'address' => 'W Taylor Street, Chicago, USA',
        ]);
    }

}