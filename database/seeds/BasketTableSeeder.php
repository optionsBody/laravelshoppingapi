<?php
/**
 * Created by PhpStorm.
 * User: adamalyan
 * Date: 15/06/15
 * Time: 23:05
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Awesome\Basket;

class BasketTableSeeder extends Seeder {

    public function run()
    {
        DB::table('basket')->delete();

        Basket::create([
            'total' => 0,
            'customer_id' => 1,
            'number_of_items' => 0,
        ]);
        Basket::create([
            'total' => 0,
            'customer_id' => 2,
            'number_of_items' => 0,
        ]);
        Basket::create([
            'total' => 0,
            'customer_id' => 3,
            'number_of_items' => 0,
        ]);
        Basket::create([
            'total' => 0,
            'customer_id' => 4,
            'number_of_items' => 0,
        ]);
    }
}