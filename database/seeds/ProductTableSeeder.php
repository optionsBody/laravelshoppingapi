<?php
/**
 * Created by PhpStorm.
 * User: adamalyan
 * Date: 07/05/15
 * Time: 19:52
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Awesome\Product;

class ProductTableSeeder extends Seeder {

    public function run()
    {
        DB::table('product')->delete();

        Product::create([
            'name' => 'ps4',
            'price' => '0350.00',
        ]);
        Product::create([
            'name' => 'xbox 360',
            'price' => '0380.00',
        ]);
        Product::create([
            'name' => 'Need 4 Speed',
            'price' => '0030.99',
        ]);
    }

}