<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('basket', function(Blueprint $table)
		{
			$table->increments('id');
            $table->decimal('total', 8, 2)->default('0.0');
            $table->integer('number_of_items', false, false)->default(0);
            $table->integer('customer_id', false, true);
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->unique('customer_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('basket');
	}

}