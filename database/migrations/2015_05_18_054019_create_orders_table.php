<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order', function(Blueprint $table)
		{
			$table->increments('id');
            $table->decimal('total', 8, 2);
            $table->integer('customer_id', false, true);
            $table->enum('status', ['unpaid', 'paid', 'cancelled'])->default('unpaid');
            $table->foreign('customer_id')->references('id')->on('customer');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order');
	}

}
