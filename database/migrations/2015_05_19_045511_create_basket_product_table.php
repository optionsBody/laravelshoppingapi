<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('basket_product', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('basket_id', false, true);
            $table->integer('product_id', false, true);
            $table->integer('quantity', false, true);
            $table->foreign('basket_id')->references('id')->on('basket');
            $table->foreign('product_id')->references('id')->on('product');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('basket_product');
	}

}
