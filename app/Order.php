<?php namespace Awesome;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $table = 'order';
    protected $fillable = array('total', 'customer_id');
    protected static $rules = [
        'total' => 'required|digits_between:000000.00,999999.99',
        'customer_id' => 'required|digits_between:1,9999999999',
        'status' => 'sometimes|required|String|in:unpaid,paid,cancelled',
    ];

    public static function validateFields($data)
    {
        $rulesToValidate = [];

        foreach($data as $field => $value){
            $rulesToValidate[$field] = static::$rules[$field];
        }

        $validator = Validator::make($data,$rulesToValidate);
        if ($validator->fails()){
            return false;
        }

        return true;
    }

    public static function getRules()
    {
        return self::$rules;
    }

    public function customer()
    {
        return $this->belongsTo('Awesome\Customer');
    }

//    public function product()
//    {
//        return $this->belongsToMany('App\Product', 'order_product', 'order_id');
//    }
    public function product()
    {
        return $this->belongsToMany('Awesome\Product', 'order_product', 'order_id', 'product_id')->withPivot('quantity')->withTimestamps();
    }

    public function delivery()
    {
     return $this->hasOne('Awesome\Delivery');
    }

    public function payment()
    {
        return $this->hasOne('Awesome\Payment');
    }

    /**
     * @param $id
     * @return bool
     */
    public static function updateTotal($id)
    {
        $order = Order::find($id);

        $total = 0;
        $products = $order->product;

        if (!$products->isEmpty()){
            foreach ($products as $item) {
                $total = $total + ($item->price * $item->pivot->quantity);
            }

        }
        $order->total = $total;

        return $order->save();
    }

    public static function setToPaid($id)
    {
        $order = Order::find($id);
        $order->status = 'paid';

        return $order->save;
    }

}
