<?php namespace Awesome;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model {

    protected $table = 'basket';
    protected $fillable = array('total', 'customer_id', 'number_of_items');
    protected $hidden = ['created_at'];
    protected static $rules = [
        'total' => 'required|digits_between:000000.00,999999.99',
        'customer_id' => 'required|digits_between:1,9999999999',
        'number_of_items' => 'required|digits_between:1,9999999999',
    ];

    public static function validateFields($data)
    {
        $rulesToValidate = [];

        foreach($data as $field => $value){
            $rulesToValidate[$field] = static::$rules[$field];
        }

        $validator = Validator::make($data,$rulesToValidate);
        if ($validator->fails()){
            return false;
        }

        return true;
    }

    public static function getRules()
    {
        return self::$rules;
    }

    public function customer()
    {
        return $this->belongsTo('Awesome\Customer', 'customer_id');
    }

    public function product()
    {
        return $this->belongsToMany('Awesome\Product', 'basket_product', 'basket_id', 'product_id')->withPivot('quantity')->withTimestamps();
    }

    /**
     * @param $id
     * @return bool
     */
    public static function updateTotal($id)
    {
        $basket = Basket::find($id);

        $total = 0;
        $itemCount = 0;
        $products = $basket->product;

        if(!$products->isEmpty()){
            foreach($products as $item){
                $total = $total + ($item->price * $item->pivot->quantity);
                $itemCount = $itemCount + $item->pivot->quantity;
            }
        }

        $basket->total = $total;
        $basket->number_of_items = $itemCount;

        return $basket->save();
    }

    public static function deleteItems($id)
    {

        $basket = Basket::find($id);

        //remove all products from basket
        $basket->product()->detach();
        //set total to zero
        $basket->total = 0;
        $basket->number_of_items = 0;
        $basket->save();
    }

}
