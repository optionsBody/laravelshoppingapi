<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::options('/', 'RootController@describe');
Route::get('/', 'RootController@index');

// customer
Route::options('customer/{id?}', 'CustomerController@describe')->where('id', '[0-9]+');
Route::resource('customer', 'CustomerController',
    ['except' => ['create', 'edit']]
);

// product
Route::options('product/{id?}', 'ProductController@describe')->where('id', '[0-9]+');
Route::resource('product', 'ProductController',
    ['except' => ['create', 'edit']]
);

// Basket
Route::options('customer/{customer_id}/basket',
    ['as' => 'basket/{id?}', 'uses' => 'BasketController@describe']
);

Route::options('basket/{id?}', 'BasketController@describe');
Route::match(['POST', 'PUT', 'PATCH'], 'customer/{customer_id}/basket',
    ['as' => 'basket/{id}', 'uses' => 'BasketController@update']
);
Route::match(['POST', 'PUT', 'PATCH'], 'basket/{id}', 'BasketController@update');
Route::get('customer/{customer_id}/basket',
    ['as' => 'basket/{id}', 'uses' => 'BasketController@show']
);
Route::delete('customer/{customer_id}/basket',
    ['as' => 'basket/{id}', 'uses' => 'BasketController@destroy']
);
Route::resource('basket', 'BasketController',
    ['except' => ['create', 'edit', 'store']]
);

// BasketProduct
Route::delete('customer/{customer_id}/basket/product/{product}',
    ['as' => 'basket/{basket}/product/{product}', 'uses' => 'BasketProductController@destroy']
);
Route::get('customer/{customer_id}/basket/product/{product}',
    ['as' => 'basket/{basket}/product/{product}', 'uses' => 'BasketProductController@show']
);
Route::patch('customer/{customer_id}/basket/product/{product}',
    ['as' => 'basket/{basket}/product/{product}', 'uses' => 'BasketProductController@update']
);
Route::options('customer/{customer_id}/basket/product/{product}',
    ['as' => 'basket/{basket}/product/{product}', 'uses' => 'BasketProductController@describe']
);
Route::options('basket/{basket}/product/{product}', 'BasketProductController@describe');
Route::match(['PATCH'], 'basket/{basket}/product/{product}', 'BasketProductController@update');
Route::resource('basket.product', 'BasketProductController',
    ['except' => ['index', 'create', 'edit', 'store', 'update']]
    );

// OrderProduct
Route::options('order/{order}/product/{product}', 'OrderProductController@describe');
Route::patch('order/{order}/product/{product}', 'OrderProductController@update');
Route::delete('order/{order}/product/{product}', 'OrderProductController@destroy');
Route::get('order/{order}/product/{product}', 'OrderProductController@show');

// delivery
Route::options('order/{order}/delivery', 'DeliveryController@describe');
Route::options('delivery/{delivery?}', 'DeliveryController@describe');
Route::get('order/{order}/delivery', 'DeliveryController@show');
Route::get('delivery/{delivery}',
    ['as' => 'order/{order}/delivery', 'uses' => 'DeliveryController@show']
);
Route::get('order', 'DeliveryController@index');
Route::post('order/{order}/delivery', 'DeliveryController@store');
Route::patch('order/{order}/delivery', 'DeliveryController@update');

// payment
Route::options('order/{order}/payment', 'PaymentController@describe');
Route::options('payment/{payment?}', 'PaymentController@describe');
Route::get('order/{order}/payment', 'PaymentController@show');
Route::get('payment/{payment}',
    ['as' => 'order/{order}/payment', 'uses' => 'PaymentController@show']
);

Route::get('payment', 'PaymentController@index');
Route::post('order/{order}/payment', 'PaymentController@store');
Route::delete('order/{order}/payment', 'PaymentController@destroy');

// order
Route::options('order/{id?}', 'OrderController@describe');
Route::options('customer/{customer_id}/order/{id}',
    ['as' => 'order/{id?}', 'uses' => 'OrderController@describe']
);

Route::post('/basket/{basket}/order', 'OrderController@store');
Route::post('/customer/{customer_id}/basket/order', 'OrderController@store',
    ['as' => '/basket/{basket}/order', 'uses' => 'OrderController@store']
);

Route::get('customer/{customer_id}/order',
    ['as' => 'order', 'uses' => 'OrderController@index']
);
Route::get('customer/{customer_id}/order/{order_id}',
    ['as' => 'order/{id}', 'uses' => 'OrderController@show']
)->where('customer_id', '[0-9]+', 'order_id', '[0-9]+');

Route::match(['PATCH'], 'order/{id}', 'OrderController@update');
Route::match(['PATCH'], 'customer/{customer_id}/order/{order_id}',
    ['as' => 'order/{id}', 'uses' => 'OrderController@update']
);
Route::resource('order', 'OrderController',
    ['only' => ['index', /*'store', 'destroy',*/ 'update', 'show']])
;

