<?php namespace Awesome\Http\Controllers;

use Illuminate\Http\Request;
use Awesome\Http\Requests;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Nocarrier\Hal;
use Illuminate\Http\Response;
use Validator;

class RootController extends BaseController {


    protected $resourceAllow = [
        'GET' => 0,
        'HEAD' => 0,
    ];

    protected $collectionAllow = [
        'GET' => 0,
        'HEAD' => 0,
    ];

    protected function getDescribeBody($httpMethod, $id=null)
    {
        $body = [];

        return $body;
    }

    public function describe($id=null)
    {
        //$excludeBodyMethods = ['GET','HEAD'];
        $body = '';
        $responseCode = 204;

        $allow = $this->collectionAllow;

        $describeBody = [];
        foreach($allow as $method => $requirement){
            if($requirement != 0){
                $key = $method;
                if($requirement==2){
                    $key = $method . '*';
                }
                $describeBody[$key] = $this->getDescribeBody($method, $id);
            }
        }

        if($describeBody){
            $body = $describeBody;
            $responseCode = 200;
        }
        $body = json_encode($body);

        $response = new Response($body,$responseCode);
        $response->header('Allow', implode(',', array_keys($allow)));

        return $response;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $self = '/';

        $resources = [
            'customer' => '/customer/{customer_id}',
            'product' => '/product/{product_id}',
            'basket' => '/basket/{basket_id}',
            'order' => '/order/{order_id}',
            'payment' => '/payment/{payment_id}',
            'delivery' => '/delivery/{delivery_id}'
        ];

        //if the client do need the pagination uri's
        //$root = FacadRequest::root();

        //create hal object
        $hal = new Hal($self, []);

        //curies link
        $hal->addLink(
            'curies',
            '/docs/rels/{rel}', //TODO make the link to docs
            [
                'name' => trim($this->curiesPrefix, ':'), //remove trailing ":"
                'templated' => true
            ],
            true //force array
        );

        foreach($resources as $resourceName => $link){
            $hal->addLink($this->curiesPrefix . $resourceName, $link, ['templated' => true]);
        }

//        foreach($resources as $resourceName => $link){
//            $resource = new Hal('/' . $link, []);
//            $hal->addResource($this->curiesPrefix . $resourceName, $resource);
//        }

        //embed data as resources, because the collection is also a resource
        //$this->embedResource($hal, $data);

        return new Response($hal->asJson(), 200, ['Content-Type' => 'application/hal+json']);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
