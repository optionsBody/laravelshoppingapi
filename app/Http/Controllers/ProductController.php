<?php namespace Awesome\Http\Controllers;

use Awesome\Http\Requests;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Nocarrier\Hal;
use Awesome\Product;
use Illuminate\Http\Response;
use Validator;

class ProductController extends BaseController {

    protected $resourceAllow = array(
        'GET' => 0,
        'HEAD' => 0,
        'PUT' => 1,
        'PATCH' => 1,
        'DELETE' => 0
    );

    protected $collectionAllow = array(
        'GET' => 0,
        'POST' => 1,
        'HEAD' => 0,
    );

    protected $rules = [
        'name' => 'required|max:100',
        'price' => 'required|digits_between:0000.00,9999.99'
    ];

    protected function getDescribeBody($httpMethod, $id=null)
    {
        $body = [];

        switch($httpMethod){
            case 'PATCH':
                if($id){
                    $body['name'] = 'string|in:pending,shipped,delivered,cancelled';
                    $body['price'] = 'digits_between:0000.00,9999.99';
                }
                break;
            case 'PUT':
                if($id){
                    $body['name'] = 'required|string|in:pending,shipped,delivered,cancelled';
                    $body['price'] = 'required|digits_between:0000.00,9999.99';
                }
                break;
            case 'POST':
                if(!$id){
                    $body['name'] = 'required|string|in:pending,shipped,delivered,cancelled';
                    $body['price'] = 'required|digits_between:0000.00,9999.99';
                }
                break;
        }

        return $body;
    }

    /**
     * @param Product $product
     * @param int $status
     * @return Response
     */
    protected function renderProduct(Product $product, $status = 200)
    {
        $self = '/product/' . $product->id;

        $hal = new Hal($self,$product->toArray());
        $hal->addLink($this->curiesPrefix . 'addToBasket', '/customer/{customer_id}/basket', ['templated' => true]);

        return new Response($hal->asJson(), $status, ['Content-Type' => 'application/hal+json']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $extraLinks[] = [
            'rel' => 'addToBasket',
            'href' => '/customer/{customer_id}/basket',
            'attributes' => [
                'templated' => true
            ]
        ];

        $extraLinks[] = [
            'rel' => 'search',
            'href' => '/product{?name}',
            'attributes' => [
                'templated' => true
            ]
        ];

        if ($this->request->input('name')) {
            $validator = $this->validateRules(['name' => $this->request->input('name')],['name']);
            //dd($validator);
            if($validator instanceof ApiProblemResponse){
                return $validator;
            }

            $products = Product::search(['name' => $this->request->input('name')], $this->itemsPerPage);
        } else{
            $products = Product::paginate($this->itemsPerPage);
        }

        return $this->renderCollection($products, 'product', $extraLinks);//$products;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //get request payload
        $data = $this->getData();
        if($data instanceof ApiProblemResponse){
            return $data;
        }

        $validator = $this->validateRules($data,array_keys($this->rules));
        if($validator instanceof ApiProblemResponse){
            return $validator;
        }

        //validate field values
        $productValidator = Product::validateFields($data);
        if($productValidator != true){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(),
                'Check the following fields \'' . implode(',', array_keys($productValidator->failed())) . '\'' );
        }

        //create product
        $product = Product::create($data);

        return $this->renderProduct($product, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        if (!$product){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Product not found.');
        }

        return $this->renderProduct($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //get entity
        $product = Product::find($id);
        if (!$product){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Product not found.');
        }

        //get request payload
        $data = $this->getData();
        if($data instanceof ApiProblemResponse){
            return $data;
        }

        if($this->isPut()) {
            $validator = $this->validateRules($data, array_keys($this->rules));
        }else{
            $validator = $this->validateRules($data);
        }

        if($validator instanceof ApiProblemResponse){
            return $validator;
        }

        //validate field values
        $productValidator = Product::validateFields($data);
        if($productValidator != true){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(),
                'Check the following fields \'' . implode(',', array_keys($productValidator->failed())) . '\'' );
        }

        //update entity and save
        foreach($data as $field => $value){
            $product->{$field} = $value;
        }
        $product->save();

        return $this->renderProduct($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = Product::find($id);

        // if not found return 404
        if (!$entity){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Product not found.');
        }

        $deleted = $entity->delete();

        // if deleted send a 204 otherwise send a 403
        if ($deleted){
            $response = new Response(null, 204);
        } else {
            $response = new Response(null, 403);
        }

        return $response;
    }

}
