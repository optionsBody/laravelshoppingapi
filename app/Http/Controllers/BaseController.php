<?php namespace Awesome\Http\Controllers;

use Awesome\Http\Requests;
use Illuminate\Http\Response;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Awesome\lib\ResponseService;
use Illuminate\Support\Facades\Request as FacadRequest;
use Illuminate\Http\Request;
use Nocarrier\Hal;
use Validator;

class BaseController extends Controller {

    protected $responseService;
    protected $request;
    protected $itemsPerPage = 5;
    public $curiesPrefix = 'lsc:';

    public function __construct(Request $request, ResponseService $responseService)
    {
        $this->responseService = $responseService;
        $this->request = $request;
    }

    /**
     * @return array
     */
    protected function getJsonData()
    {
        $payload = $this->request->getContent();
        $data = json_decode($payload, true);

        return $data;
    }

    /**
     * @return bool
     */
    protected function hasPayload()
    {
        if($this->request->getContent())
        {
            return true;
        }
        return false;
    }

    protected function getData()
    {
        //get request payload
        try{
            $data = $this->getJsonData();
        } catch (Exception $e) {
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Payload is expected');
        }

        if (!$data){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Invalid Json format');
        }

        return $data;
    }

    /**
     * @return bool
     */
    protected function isPut()
    {
        $method = $this->request->getMethod();

        if ($method == 'PUT'){
            return true;
        } else {
           return false;
        }
    }

    /**
     * @return mixed
     */
    protected function getRequestedBasePath()
    {
        $path = $this->request->path();
        $pathParts = explode('/', $path);
        return $pathParts[0];
    }

    /**
     * Set Allow http header and returns payloads
     * format for each allowed http method
     *
     * @param null $id
     * @return Response
     */
    public function describe($id=null)
    {
        //$excludeBodyMethods = ['GET','HEAD'];
        $body = '';
        $responseCode = 204;
        if ($id){
            $allow = $this->resourceAllow;
        } else {
            $allow = $this->collectionAllow;
        }

        $describeBody = [];
        foreach($allow as $method => $requirement){
            if($requirement != 0){
                $key = $method;
                if($requirement==2){
                    $key = $method . '*';
                }
                $describeBody[$key] = $this->getDescribeBody($method, $id);
            }
        }

        if($describeBody){
            $body = $describeBody;
            $responseCode = 200;
        }
        $body = json_encode($body);

        $response = new Response($body,$responseCode);
        $response->header('Allow', implode(',', array_keys($allow)));

        return $response;
    }

    /**
     * @param int $status
     * @return $this
     */
    protected static function returnApiProblem($status = 400)
    {
        $problem = new ApiProblemResponse('', $status);
        return $problem->setData();
    }

    protected function returnNoContent()
    {
        return new Response('',204);
    }

    protected function embedResource(Hal &$hal, $data, $resourceName)
    {
        if (!$data) return false;

        //process collection items
        foreach($data as $item) {

            //when searching Csutomer::search the objects are std objects
            if($item instanceof \stdClass){
                $attributes = /*$item->get();*/json_decode(json_encode($item), true);
            }else{
                $attributes = $item->toArray();
            }

            //instantiate a new hal embedded resource
            $resource = new Hal(null, $attributes);

            //get and set the required self link (every resource have a self link)
            $self = [ 'self' => '/' . $resourceName . '/' . $item->id ];

            $resource->setUri($self['self']);

            $itemLinks = $this->getResourceLinks($attributes);

            foreach($itemLinks as $rel => $link){
                $resource->addLink($this->curiesPrefix . $rel, $link);
            }

            //for the embedded resource name, strip the namespace part and get the real pluralized class name
            $hal->addResource($this->curiesPrefix . $resourceName, $resource);
        }

        return true;
    }

    /**
     * @param array $attributes
     * @return array
     */
    protected function getResourceLinks($attributes = [])
    {
        $links = [];

        foreach($attributes as $key => $value){
            $parts = explode('_', $key);
            if(count($parts)>1 && $parts[1] == 'id'){
                $relatedResourceName = $parts[0];
                $relatedResourceId = $value;
                $links[$relatedResourceName] = '/' . $relatedResourceName . '/' . $relatedResourceId;
            }
        }

        return $links;
    }

    protected function renderCollection($data, $resourceName, $extraLinks = [])
    {
        // Prepend a string with a single instance of a given value.
        $self = '/'.ltrim(FacadRequest::path()/*, '/'*/);
        $currentPage = $data->currentPage();
        $lastPage    = $data->lastPage();

        //if the client do need the pagination uri's
        $root = FacadRequest::root();

        $first = str_replace($root, '', $data->url(1));
        $prev = ($currentPage > 1) ? str_replace($root, '', $data->url($currentPage-1)) : null;
        $next = ($currentPage < $lastPage) ? str_replace($root, '', $data->nextPageUrl()) : null;
        $last = str_replace($root, '', $data->url($lastPage));
        $page = str_replace($root, '', $data->url($currentPage));

        $paginationLinks = [
            'first'     => $first,
            'prev'      => $prev,
            'next'      => $next,
            'last'      => $last,
            'page'      => $page,
        ];

        //create hal object
        $hal = new Hal($self, [
            'numberOfItems'     => $data->total(),
            'itemsPerPage'   => $data->perPage(),
        ]);

        //curies link
        $hal->addLink(
            'curies',
            '/docs/rels/{rel}', //TODO make the link to docs
            [
                'name' => trim($this->curiesPrefix, ':'), //remove trailing ":"
                'templated' => true
            ],
            true //force array
        );

        foreach($paginationLinks as $rel => $link){
            $hal->addLink($rel, $link);
        }

        if($extraLinks){
            foreach($extraLinks as $link){
                $hal->addLink($this->curiesPrefix . $link['rel'], $link['href'], $link['attributes']);
            }
        }

        $this->embedResource($hal, $data, $resourceName);

        return new Response($hal->asJson(), 200, ['Content-Type' => 'application/hal+json']);
    }

    //Validate given rules, otherwise validate all fields
    protected function validateRules($input, $fieldsToValidate=[])
    {
        if(!$fieldsToValidate){
            $rules = [];
        }else{
            foreach($fieldsToValidate as $field){
                $rules[$field] = $this->rules[$field];
            }
        }

        $reason = 'Invalid data format';

        //build rules to validate and fields passed are valid
        foreach($input as $field => $value){
            if(!array_key_exists($field, $this->rules)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason . 'Invalid parameter');
            }
            if(!$fieldsToValidate){
                $rules[$field] = $this->rules[$field];
            }
        }

        if($fieldsToValidate){
            if(count($input) != count($rules)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason . ' Wrong number parameters');
            }
        }

        return Validator::make($input, $rules);

    }

}
