<?php namespace Awesome\Http\Controllers;

use Awesome\Delivery;
use Awesome\Http\Requests;
use Awesome\Order;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Awesome\lib\HyperResponse\Http\HalResponse;
use Awesome\Payment;
use Illuminate\Http\Response;
use Validator;
use Nocarrier\Hal;

class PaymentController extends BaseController {
    protected $resourceAllow = [
        'GET' => 0,
        'POST' => 0,
        'HEAD' => 0,
        'DELETE' => 0,
    ];

    protected $collectionAllow = [
        'GET' => 0,
        'HEAD' => 0,
    ];

    protected $rules = [
        'transaction_id' => 'required|alpha_num|size:40',
        'order_id' => 'required|digits_between:1,9999999999'
    ];

    protected function getDescribeBody($httpMethod, $id=null)
    {
        $body = [];

        return $body;
    }

    protected function renderPayment($payment, $status = 200, $extraHeaders=[])
    {
        $self = '/order/' . $payment->order_id . '/payment';

        $hal = new Hal($self,$payment->toArray());
        $hal->addLink($this->curiesPrefix . 'deliver', '/order/' . $payment->order_id . '/delivery');
        $hal->addLink($this->curiesPrefix . 'cancelPayment', '/order/' . $payment->order_id . '/payment');

        //embed basket
        $orderEmebeded = new Hal('/order/' . $payment->order_id, $payment->order->toArray());
        $hal->addResource($this->curiesPrefix . 'order', $orderEmebeded);

        //add content type header
        $headers = [];
        $headers['Content-Type'] = 'application/hal+json';

        //add extra header if set
        if($extraHeaders){
            foreach($extraHeaders as $header => $value){
                $headers[$header] = $value;
            }
        }

        return new Response($hal->asJson(), $status, $headers);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if ($this->request->input('order')) {
            $orderId = $this->request->input('order');
            $validator = $this->validateRules(['order_id' => $orderId],['order_id']);
            if($validator instanceof ApiProblemResponse){
                return $validator;
            }
            //$orders = $customer->order()->paginate($this->itemsPerPage);
            $order = Order::find($orderId);
            //dd($order);
            $payment = $order->payment;
            //dd($payments);
            return $this->renderPayment($payment);
        }else{
            $payments = Payment::paginate($this->itemsPerPage);
        }

        $extraLinks[] = [
            'rel' => 'find',
            'href' => '/payment{?order}',
            'attributes' => [
                'templated' => true
            ]
        ];

        return $this->renderCollection($payments, 'payment', $extraLinks);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($orderId)
	{
        $order = Order::find($orderId);
        if (!$order instanceof Order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        //create payment
        $transactionId = sha1(time());
        $payment = Payment::create([
            'transaction_id' => $transactionId,
            'order_id'  => $order->id
        ]);

        //update order status
        $order->status = 'paid';
        $order->save();

        $url = 'http://' . $this->request->server('SERVER_NAME') . '/payment/' . $payment->id;

        return $this->renderPayment($payment, 201, ['Location' => $url]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $basePath = $this->getRequestedBasePath();

        if ($basePath == 'payment'){
            $payment = Payment::find($id);
        } else {
            $order = Order::find($id);
            if (!$order instanceof Order) {
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
            }

            $payment = $order->payment;
        }

        if(!$payment instanceof Payment){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Payment not found.');
        }

        return $this->renderPayment($payment);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($orderId)
	{
        $order = Order::find($orderId);
        if (!$order instanceof Order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        //check payment exist
        $payment = $order->payment;
        if (!$payment instanceof Payment){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Payment not found.');
        }

        $delivery = $order->delivery;

        //check delivery
        if ($delivery instanceof Delivery && $delivery->status != 'cancelled'){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(),
                'Delivery created, can NOT delete payment without cancelling the delivery.');
        }

        $deleted = $payment->delete();
        $order->status = 'unpaid';
        $order->save();

        // if deleted send a 204 otherwise send a 500
        if ($deleted){
            $response = new Response(null, 204);
        } else {
            $response = new Response(null, 500);
        }

        return $response;
	}

}
