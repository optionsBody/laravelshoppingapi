<?php namespace Awesome\Http\Controllers;

use Awesome\Customer;
use Awesome\Order;
use Awesome\Basket;
use Awesome\Http\Requests;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Nocarrier\Hal;
use Illuminate\Http\Response;
use Validator;

class OrderController extends BaseController {

    protected $resourceAllow = [
        'GET' => 0,
        'HEAD' => 0,
        'PATCH' => 1,
    ];

    protected $collectionAllow = [
        'GET' => 0,
        'HEAD' => 0,
        'POST' => 0,
    ];

    protected $rules = [
        'total' => 'required|digits_between:000000.00,999999.99',
        'customer_id' => 'required|digits_between:1,9999999999',
        'status' => 'sometimes|required|String|in:unpaid,paid,cancelled',
    ];

    protected function getDescribeBody($httpMethod, $id=null)
    {
        $body = [];

        switch($httpMethod){
            case 'PATCH':
                if($id){
                    $body['status'] = 'String|in:unpaid,paid,cancelled';
                }
                break;
        }

        return $body;
    }

    protected function renderOrder($order, $status = 200, $extraHeaders=[])
    {
        //if id is passesd get order
        if(is_numeric($order)){
            $order = Order::find($order);
            if(!$order){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
            }
        }

        //Create hal for order
        $self = '/order/' . $order->id;
        $hal = new Hal($self,$order->toArray());
        $hal->addLink('pay', $self . '/payment');
        //$hal->addLink('orderItems', $self . '/product');
        $hal->addLink('orders', '/customer/' . $order->customer_id . '/order');
        $hal->addLink('findOrder', '/order' . '/{order}', ['templated' => true]);

        //embed customer
        //Add customer as embedded
        $customerEmbeded = new Hal('/customer/' . $order->customer_id, $order->customer->toArray());
        $hal->addResource('customer', $customerEmbeded);

        //Add products
        foreach($order->product as $product){
            $productOrder = $product->pivot->toArray();
            unset($productOrder['order_id']);//TODO why?
            $itemLink = $self . '/product/' . $productOrder['product_id'];
            $halOrderProduct = new Hal($itemLink,$productOrder);
            $halOrderProduct->addLink($this->curiesPrefix . 'removeItem', $itemLink);
            $halOrderProduct->addLink($this->curiesPrefix . 'changeQuantity', $itemLink);

            //embed product in Basket item
            $productArray = $product->toArray();
            unset($productArray['created_at']);
            unset($productArray['updated_at']);
            unset($productArray['pivot']);
            $productEmbeded = new Hal('/product/' . $productArray['id'], $productArray);
            $halOrderProduct->addResource('Product', $productEmbeded);

            //TODO keep the embeded item in basket while in order only the link
            //Add link to product instead of embeding the product
            //$halBasketProductLinks = $this->getResourceLinks($productOrder);

//            foreach($halBasketProductLinks as $rel => $link){
//                $halBasketProduct->addLink($rel, $link);
//            }

            $hal->addResource($this->curiesPrefix . 'OrderItems', $halOrderProduct);
        }

        $headers = [];
        $headers['Content-Type'] = 'application/hal+json';

        if($extraHeaders){
            foreach($extraHeaders as $header => $value){
                $headers[$header] = $value;
            }
        }

        return new Response($hal->asJson(), $status, $headers);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = null)
	{
        $basePath = $this->getRequestedBasePath();

        //get customer orders if request is /customer/{id}/order
        if ($basePath == 'customer'){
            //check customer exists
            $customer = Customer::find($id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Customer not found.');
            }
            $orders = $customer->order()->paginate($this->itemsPerPage);
            return $this->renderCollection($orders, 'orders');
        }

        //get customer orders if request is /order?customer={customer}
        if ($basePath == 'order' && $this->request->input('customer')) {
            $customer_id = $this->request->input('customer');
            $validator = Validator::make(['customer' => $customer_id], ['customer' => 'required|integer']);

            if ($validator->fails()) {
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Query parameter is not valid.');
            }
            $customer = Customer::find($customer_id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Customer not found.');
            }
            $orders = $customer->order()->paginate($this->itemsPerPage);
            return $this->renderCollection($orders, 'orders');
        }

        //get all orders if request is /order
        if ($basePath == 'order'){
            $orders = Order::paginate($this->itemsPerPage);
        }
        $extraLinks[] = [
            'rel' => 'find',
            'href' => '/order{?customer}',
            'attributes' => [
                'templated' => true
            ]
        ];

        return $this->renderCollection($orders, 'orders', $extraLinks);
	}

    public function getOrderTotal(Basket $basket)
    {
        $total = 0;
        $products = $basket->product;

        if(!$products->isEmpty()){
            foreach($products as $item){
                $total = $total + ($item->price * $item->pivot->quantity);
            }
            return $total;
        }

        return $total;
    }

    /**
     * @param $id
     * @return $this|Response
     */
	public function store($id)
	{
        $basePath = $this->getRequestedBasePath();

        if ($basePath == 'customer'){
            //check customer exists
            $customer = Customer::find($id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo());
            }
            // get basket
            $basket = $customer->basket;
        } else {
            //get basket
            $basket = Basket::find($id);
        }

        if (!$basket){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo());
        }

        $total = $this->getOrderTotal($basket);

        if(!$total){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'No products in Basket.');
        }

        //Create product
        $order = Order::create([
            'total' => $total,
            'customer_id' => $basket->customer_id,
        ]);

        if(!$order instanceof Order){
            return ApiProblemResponse::start(500)->setData('', $this->request->getPathInfo());
        }

        //add products to basket
        $products = $basket->product;

        foreach ($products as $product) {
            $order->product()->attach($product->id, ['quantity' => $product->pivot->quantity]);
        }

        //empty basket
        Basket::deleteItems($basket->id);

        $url = 'http://' . $this->request->server('SERVER_NAME') . '/order/' . $order->id;

        return $this->renderOrder($order, 201, ['Location' => $url]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, $Orderid=null)
	{
        $basePath = $this->getRequestedBasePath();

        if ($basePath == 'customer'){
            //check customer exists
            $customer = Customer::find($id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo());
            }

            //get order or create
            $order = $customer->order()->where('id', $Orderid)->first();
            //$order = Order::firstOrCreate(['customer_id' => $id]);
        } else {
            //get order
            $order = Order::find($id);
        }

        if (!$order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        return $this->renderOrder($order);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, $orderId = null)
	{
        $basePath = $this->getRequestedBasePath();

        //not accepting any query parameters
        if($this->request->all()){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Resource does Not accept query parameters');
        }

        if ($basePath == 'customer'){
            //check customer exists
            $customer = Customer::find($id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo());
            }
            // get basket
            $order = $customer->order()->where('id', $orderId)->first();
        } else {
            //get basket
            $order = Order::find($id);
        }

        if(!$order instanceof Order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        $data = $this->getData();
        if($data instanceof ApiProblemResponse){
            return $data;
        }

        $expect = ['status'];
        $reason = 'Invalid data format';

        //validation
        $validator = $this->validateRules($data,['status']);
        if($validator instanceof ApiProblemResponse){
            return $validator;
        }

        if($order->status == $data['status']){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Order Status is \'' . $order->status . '\' already.');
        }

        if($order->payment){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Order has been paid for, please cancel payment first');
        }

        $delivery = $order->delivery;
        if($delivery && $delivery->status != 'cancelled'){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Please cancel order delivery first');
        }

        //change status
        $order->status = $data['status'];
        $save = $order->save();

        if($save){
            return $this->renderOrder($order->id);
        } else {
            return new Response(null, 500);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, $orderId = null)
	{
        $basePath = $this->getRequestedBasePath();

        if ($basePath == 'customer'){
            //check customer exists
            $customer = Customer::find($id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo());
            }
            // get basket
            $order = $customer->order()->where('id', $orderId)->first();
        } else {
            //get basket
            $order = Order::find($id);
        }

        if(!$order instanceof Order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        if($order->status == 'paid'){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Order has been paid for, please cancel payment first');
        }

        if($order->status == 'cancelled'){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Order Status is already \'cancelled\'.');
        }


        //set status to cancelled
        $order->status = 'cancelled';

        $order->save();

        return $this->renderOrder($order->id, 204);

//        // if deleted send a 204 otherwise send a 403
//        if ($deleted){
//            $response = new Response(null, 202);
//        } else {
//            $response = new Response(null, 500);
//        }
//
//        return $response;
	}

}
