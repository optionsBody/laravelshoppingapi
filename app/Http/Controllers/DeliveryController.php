<?php namespace Awesome\Http\Controllers;

use Awesome\Delivery;
use Awesome\Http\Requests;
use Awesome\Order;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Awesome\lib\HyperResponse\Http\HalResponse;
use Illuminate\Http\Response;
use Validator;
use Nocarrier\Hal;

class DeliveryController extends BaseController {

    protected $resourceAllow = [
        'GET' => 0,
        'POST' => 0,
        'PATCH' => 1,
        'HEAD' => 0,
        //'DELETE' => 0,
    ];

    protected $collectionAllow = [
        'GET' => 0,
        'HEAD' => 0,
    ];

    protected $rules = [
        'status' => 'required|string|in:pending,shipped,delivered,cancelled',
    ];

    protected function getDescribeBody($httpMethod, $id=null)
    {
        $body = [];

        switch($httpMethod){
            case 'PATCH':
                if($id){
                    $body['status'] = 'required|string|in:pending,shipped,delivered,cancelled';
                }
                break;
        }

        return $body;
    }

    /**
     * @param Delivery $delivery
     * @param int $status
     * @return Response
     */
    protected function renderDelivery(Delivery $delivery, $status = 200, $extraHeaders=[])
    {
        $self = '/order/' . $delivery->order_id . '/delivery';

        $hal = new Hal($self,$delivery->toArray());
        $hal->addLink($this->curiesPrefix . 'deliver', '/order/' . $delivery->order_id . '/delivery');
        $hal->addLink($this->curiesPrefix . 'cancelDelivery', '/order/' . $delivery->order_id . '/delivery');

        //embed basket
        $orderEmebeded = new Hal('/order/' . $delivery->order_id, $delivery->order->toArray());
        $hal->addResource($this->curiesPrefix . 'order', $orderEmebeded);

        //add content type header
        $headers = [];
        $headers['Content-Type'] = 'application/hal+json';

        //add extra headers if set
        if($extraHeaders){
            foreach($extraHeaders as $header => $value){
                $headers[$header] = $value;
            }
        }

        return new Response($hal->asJson(), $status, $headers);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($orderId)
	{
        $order = Order::find($orderId);
        if (!$order instanceof Order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        $orderStatus = $order->status;

        if ($orderStatus != 'paid' ){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(),
                'Can NOT create delivery, Order status is \'' . $orderStatus . '\'.');
        }

        //create delivery
        $delivery = Delivery::create([
            'status' => 'pending',
            'order_id'  => $order->id
        ]);

        //update order status
        $order->status = 'paid';
        $order->save();

        $url = 'http://' . $this->request->server('SERVER_NAME') . '/delivery/' . $delivery->id;

        return $this->renderDelivery($delivery, 201, ['Location' => $url]) ;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $basePath = $this->getRequestedBasePath();

        if ($basePath == 'delivery'){
            $delivery = Delivery::find($id);
        } else {
            $order = Order::find($id);
            if (!$order instanceof Order) {
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
            }
            $delivery = $order->delivery;
        }

        if(!$delivery instanceof Delivery){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Delivery not found.');
        }

        return $this->renderDelivery($delivery);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($orderId)
	{
        $order = Order::find($orderId);
        if (!$order instanceof Order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        //check delivery exist
        $delivery = $order->delivery;
        if (!$delivery instanceof Delivery){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Delivery not found.');
        }

        //get data
        $data = $this->getData();
        if($data instanceof ApiProblemResponse){
            return $data;
        }

        //validation
        $validator = $this->validateRules($data,['status']);
        if($validator instanceof ApiProblemResponse){
            return $validator;
        }

        if($delivery->status == $data['status']){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Delivery Status is \'' . $order->status . '\' already.');
        }

        $delivery->status = $data['status'];
        $save = $delivery->save();

        if($save) {
            return $this->renderDelivery($delivery);
        } else {
            return new Response(null, 500);
        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($orderId)
	{
        $order = Order::find($orderId);
        if (!$order instanceof Order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        //check delivery exist
        $delivery = $order->delivery;
        if (!$delivery instanceof Delivery){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Delivery not found.');
        }

        //check delivery has not been cancelled already
        if ($delivery->status == 'cancelled'){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(),
                'Delivery is cancelled already.');
        }

        //check delivery
        if ($delivery->status != 'pending'){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(),
                'Delivery can NOT be cancelled as it is has been ' . $delivery->status);
        }

        $delivery->status = 'cancelled';
        $order->status = 'unpaid';
        $save = $order->save();

        // if deleted send a 204 otherwise send a 500
        if ($save){
            $response = new Response(null, 204);
        } else {
            $response = new Response(null, 500);
        }

        return $response;
	}

}
