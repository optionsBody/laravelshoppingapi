<?php namespace Awesome\Http\Controllers;

use Awesome\Http\Requests;
use Awesome\Customer;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Nocarrier\Hal;
use Illuminate\Http\Response;
use Awesome\Basket;
use Validator;

class BasketProductController extends BaseController {

    //Body: 0 = Not Required; 1 = Required ; 2 = Optional
    protected $resourceAllow = [
        'GET' => 0, // get basket
        'HEAD' => 0,
        //'PUT' => 1, // put a basketProduct representation ie. update quantity
        'PATCH' => 1, // replace product in basket
        'DELETE' => 0// delete product from basket
    ];

    protected $collectionAllow = [
        //'GET' => 0,
        //'POST',
        //'HEAD' => 0,
    ];

    protected function getDescribeBody($httpMethod, $id=null)
    {
        $body = [];

        switch($httpMethod){
            case 'PATCH':
            if($id){
                $body['quantity'] = 'required|integer';
            }
            break;
        }

        return $body;
    }

    protected function renderBasketProduct($basket, $product)
    {
        $self = '/basket/' . $basket->id . '/product/' . $product->id;

        $basketProduct = $product->pivot->toArray();
        //dd($basketProduct);
        $hal = new Hal($self,$basketProduct);
        $hal->addLink($this->curiesPrefix . 'deleteItem' , $self);
        $hal->addLink($this->curiesPrefix . 'changeQuantity' , $self);

        //embed product
        $embedProduct = $product->toArray();
        unset($embedProduct['pivot']);
        //

        $halEmbed = new Hal('/product/' . $product->id, $embedProduct);

        $hal->addResource($this->curiesPrefix .'product' , $halEmbed);
        $hal->addLink($this->curiesPrefix . 'basket', '/basket/' . $basket->id );
        $hal->addLink($this->curiesPrefix . 'customer', '/customer/' . $basket->customer_id );

        return new Response($hal->asJson(), 200, ['Content-Type' => 'application/hal+json']);
    }

    protected function getBasket($id, $productId)
    {
        //Get Basket
        $basePath = $this->getRequestedBasePath();

        if ($basePath == 'customer'){
            //check customer exists
            $customer = Customer::find($id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo());
            }

            // get basket or create
            $basket = Basket::firstOrCreate(['customer_id' => $id]);
        } else {
            //get basket
            $basket = Basket::find($id);
        }

        if (!$basket){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Basket not found.');
        }

        //get basket products
        $productsInBasket = $basket->product;
        if(!$productsInBasket->contains($productId)){//dd($productsInBasket);
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Product not in Basket.');
        }

        return $basket;//->product;//()->find($product);
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, $productId)
	{
        $basket = $this->getBasket($id, $productId);
        if($basket instanceof ApiProblemResponse){
            return $basket;
        }

        $product = $basket->product()->find($productId);

        return $this->renderBasketProduct($basket, $product);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, $productId)
	{
        $basket = $this->getBasket($id, $productId);
        if($basket instanceof ApiProblemResponse){
            return $basket;
        }
        $product = $basket->product()->find($productId);

        //get data
        $obj = $this->getData();
        if($obj instanceof ApiProblemResponse){
            return $obj;
        }

        $expect = ['quantity'];
        $reason = 'Invalid data format';

        //validation
        if(count($obj) != count($expect)){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
        }
        foreach ($obj as $field => $value){
            if (!in_array($field, $expect)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
            }
            if(!is_numeric($value)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
            }
        }

        //remove product
        $basket->product()->detach([$productId]);
        //add product
        $basket->product()->attach($productId, ['quantity' => $obj['quantity']]);

        //update basket total
        if(!Basket::updateTotal($id)){
            return ApiProblemResponse::start(500)->setData('', $this->request->getPathInfo(), 'Basket total not updated');
        }

        return $this->show($id, $productId);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, $productId)
	{
        $basket = $this->getBasket($id, $productId);
        if($basket instanceof ApiProblemResponse){
            return $basket;
        }

        if($this->hasPayload()){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Resource method does Not accept a body.');
        }

        $basket->product()->detach([$productId]);

        //update basket total
        if(!Basket::updateTotal($id)){
            return ApiProblemResponse::start(500)->setData('', $this->request->getPathInfo(), 'Basket total not updated');
        }

        return new Response(null, 204);
	}

}
