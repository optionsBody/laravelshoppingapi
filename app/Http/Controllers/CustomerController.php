<?php namespace Awesome\Http\Controllers;

use Awesome\Http\Requests;
use Awesome\Customer;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Awesome\lib\HyperResponse\Http\HalResponse;
use Illuminate\Http\Response;
use Validator;
use Nocarrier\Hal;


class CustomerController extends BaseController {

    /**
     * 0 = Method allowed, but with no data in request body
     * 1 = Method allowed, with data in request body
     * 2 = Method allowed, request body is optional
     */
    protected $resourceAllow = [
        'GET' => 0,
        'HEAD' => 0,
        'PUT' => 1,
        'PATCH' => 1,
        'DELETE' => 0,
    ];

    protected $collectionAllow = [
        'GET' => 0,
        'POST' => 1,
        'HEAD' => 0,
        'DELETE' => 2,
    ];

    protected $rules = [
        'first_name' => 'required|string|max:100',
        'last_name' => 'required|string|max:100',
        'email' => 'required|string|max:128',
        'address' => 'required|string|max:256',
    ];

    protected function getDescribeBody($httpMethod, $id=null)
    {
        $body = [];

        switch($httpMethod){
            case 'POST':
                if(!$id){
                    $body[] =[
                        'first_name' => 'required|max:100',
                        'last_name' => 'required|max:100',
                        'email' => 'required|unique|email',
                        'address' => 'required'
                    ];
                }
                break;
            case 'PUT':
                if($id){
                    $body['first_name'] = 'required|max:100';
                    $body['last_name'] = 'required|max:100';
                    $body['email'] = 'required|email';
                    $body['address'] = 'required';
                }
                break;
            case 'PATCH':
                if($id){
                    $body['first_name'] = 'max:100';
                    $body['last_name'] = 'max:100';
                    $body['email'] = 'email';
                    $body['address'] = '';
                }
                break;
            case 'DELETE':
                if(!$id) {
                    $body['customers'] = 'required|array';
                }
                break;
        }

        return $body;
    }

    //Validate given rules, otherwise validate all fields
    protected function validateRules($input, $fieldsToValidate=[])
    {
        if(!$fieldsToValidate){
            $rules = [];
        }else{
            foreach($fieldsToValidate as $field){
                $rules[$field] = $this->rules[$field];
            }
        }

        $reason = 'Invalid data format';

        //build rules to validate and fields passed are valid
        foreach($input as $field => $value){
            if(!array_key_exists($field, $this->rules)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason . 'Invalid parameter');
            }
            if(!$fieldsToValidate){
                $rules[$field] = $this->rules[$field];
            }
        }

        if($fieldsToValidate){
            if(count($input) != count($rules)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason . ' Wrong number parameters');
            }
        }

        return Validator::make($input, $rules);

    }

    protected function renderCustomer($customer, $status = 200)
    {
        $self = '/customer/' . $customer->id;

        $hal = new Hal($self,$customer->toArray());
        $hal->addLink('basket', $self . '/basket');
        $hal->addLink('orders', $self . '/orders');
        $hal->addLink('findOrder', $self . '/orders/{order}', ['templated' => true]);

        return new Response($hal->asJson(), $status, ['Content-Type' => 'application/hal+json']);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $extraLinks[] = [
            'rel' => 'find',
            'href' => '/customer{?first_name}{?last_name}{?email}{?address}',
            'attributes' => [
                'templated' => true
            ]
        ];

        if ($this->request->all()) {
            $input = $this->request->all();

            $inputNoPage = $input;
            if(isset($inputNoPage['page'])){
                unset($inputNoPage['page']);
            }

            $validator = $this->validateRules($inputNoPage);
            if($validator instanceof ApiProblemResponse){
                return $validator;
            }

            if (!$validator->fails()) {
                $customers = Customer::search($input,$this->itemsPerPage);
                if (!$customers){
                    return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Customer not found.');
                }

                return $this->renderCollection($customers, 'customer', $extraLinks);
            } else {
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Bad request.');
            }
        }

        $customers = Customer::paginate($this->itemsPerPage);

        return $this->renderCollection($customers, 'customer', $extraLinks);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

        $data = $this->getData();
        if($data instanceof ApiProblemResponse){
            return $data;
        }

        $validator = $this->validateRules($data,array_keys($this->rules));
        if($validator instanceof ApiProblemResponse){
            return $validator;
        }

        //validate field values
        $customerValidator = Customer::validateFields($data);
        if($customerValidator !== true){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(),
                'Check the following fields \'' . implode(',', array_keys($customerValidator->failed())) . '\'' );
        }

        //create customer
        $customer = Customer::create($data);
        //TODO check customer has been created


        return $this->renderCustomer($customer, 201);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $customer = Customer::find($id);
        if (!$customer){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Customer not found.');
        }

        return $this->renderCustomer($customer);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        //get entity
        $customer = Customer::find($id);
        if (!$customer){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Customer not found.');
        }

        $badRequest = new Response('',400);
        //get entity fields
        $fields = $customer->getFillable();

        $data = $this->getData();
        if($data instanceof ApiProblemResponse){
            return $data;
        }

        if($this->isPut()) {
            $validator = $this->validateRules($data, array_keys($this->rules));
        }else{
            $validator = $this->validateRules($data);
        }

        if($validator instanceof ApiProblemResponse){
            return $validator;
        }

        //validate field values
        if(!$customer->validateFields($data)){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Bad request.');
        }

        //update entity and save
        foreach($data as $field => $value){
            $customer->{$field} = $value;
        }
        $customer->save();

        return $this->renderCustomer($customer);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$entity = Customer::find($id);

        // if not found return 404
        if (!$entity){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Customer not found.');
        }

        $deleted = $entity->delete();

        // if deleted send a 204 otherwise send a 403
        if ($deleted){
            $response = new Response(null, 204);
        } else {
            $response = new Response(null, 500);
        }

        return $response;
	}

}
