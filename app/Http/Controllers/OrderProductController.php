<?php namespace Awesome\Http\Controllers;

use Awesome\Http\Requests;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Nocarrier\Hal;
use Validator;
use Awesome\Order;
use Illuminate\Http\Response;

class OrderProductController extends BaseController {

    //Body: 0 = Not Required; 1 = Required ; 2 = Optional
    protected $resourceAllow = [
        'GET' => 0, // get OrderProduct
        'HEAD' => 0,
        'PATCH' => 1, // modify quantity
        'DELETE' => 0// remove product from order
    ];

    protected $collectionAllow = [
        //'GET' => 0,
        //'POST',
        //'HEAD' => 0,
    ];

    protected function getDescribeBody($httpMethod, $id=null)
    {
        $body = [];

        switch($httpMethod){
            case 'PATCH':
                if($id){
                    $body['quantity'] = 'required|integer';
                }
                break;
        }

        return $body;
    }

    protected function renderOrderProduct($order, $product, $status = 200)
    {
        $self = '/order/' . $order->id . '/product/' . $product->id;

        $orderProduct = $product->pivot->toArray();
        //dd($orderProduct);
        $hal = new Hal($self,$orderProduct);
        $hal->addLink('deleteItem' , $self);
        $hal->addLink('changeQuantity' , $self);

        //embed product
        $embedProduct = $product->toArray();
        unset($embedProduct['pivot']);
        //

        $halEmbed = new Hal('/product/' . $product->id, $embedProduct);

        $hal->addResource('product' , $halEmbed);
        $hal->addLink($this->curiesPrefix . 'order', '/order/' . $order->id );
        $hal->addLink($this->curiesPrefix . 'customer', '/customer/' . $order->customer_id );

        return new Response($hal->asJson(), $status, ['Content-Type' => 'application/hal+json']);
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($orderId, $productId)
	{
        $order = Order::find($orderId);

        if (!$order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        //get basket products
        $productsInOrder = $order->product;
        if(!$productsInOrder->contains($productId)){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order Item not found.');
        }

        $product = $order->product()->find($productId);

        return $this->renderOrderProduct($order, $product);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($orderId, $productId)
	{
        $order = Order::find($orderId);

        if (!$order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        //get basket products
        $productsInOrder = $order->product;
        if(!$productsInOrder->contains($productId)){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order Item not found.');
        }

        //get data
        $obj = $this->getData();
        if($obj instanceof ApiProblemResponse){
            return $obj;
        }

        $expect = ['quantity'];
        $reason = 'Invalid data format';

        //validation
        if(count($obj) != count($expect)){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
        }
        foreach ($obj as $field => $value){
            if (!in_array($field, $expect)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
            }
            if(!is_numeric($value)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
            }
        }

        //check order is not paid or cancelled
        if($order->status == 'cancelled' || $order->status == 'paid' ){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(),
                'Product quantity can NOT be modified. Order status is ' . $order->status);
        }

        //remove product
        $order->product()->detach([$productId]);
        //add product
        $order->product()->attach($productId, ['quantity' => $obj['quantity']]);

        //update basket total
        if(!Order::updateTotal($orderId)){
            return ApiProblemResponse::start(500)->setData('', $this->request->getPathInfo(), 'Order Total and NumberOf items not updated');
        }

        return $this->show($orderId, $productId);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($orderId, $productId)
	{
        $order = Order::find($orderId);

        if (!$order){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order not found.');
        }

        //get basket products
        $productsInOrder = $order->product;
        if(!$productsInOrder->contains($productId)){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Order Item not found.');
        }

        //check order is not paid or cancelled
        if($order->status == 'cancelled' || $order->status == 'paid' ){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(),
                'Product can NOT be removed from Order. Order status is ' . $order->status);
        }

        //remove product from order
        $order->product()->detach($productId);

        //update basket total
        if(!Order::updateTotal($orderId)){
            return ApiProblemResponse::start(500)->setData('', $this->request->getPathInfo(), 'Order Total and NumberOf items not updated');
        }

        return new Response(null, 204);
	}

}
