<?php namespace Awesome\Http\Controllers;

use Awesome\Customer;
use Awesome\Http\Requests;
use Awesome\lib\HyperResponse\Http\ApiProblemResponse;
use Nocarrier\Hal;
use Awesome\Product;
use Illuminate\Http\Response;
use Awesome\Basket;
use Validator;

class BasketController extends BaseController {

    protected $resourceAllow = [
        'GET' => 0, // get basket
        'POST' => 1, // add new product
        'HEAD' => 0,
        'PUT' => 1, // put a basket representation
        'PATCH' => 1, // update basket field (don't allow price)
        'DELETE' => 2// delete product from basket OR empty basket
    ];

    protected $collectionAllow = [
        'GET' => 0,
        //'POST',
        'HEAD' => 0,
    ];

    protected function getDescribeBody($httpMethod, $id=null)
    {
        $body = [];

        switch($httpMethod){
            case 'POST':case 'PUT':case 'PATCH':
                if($id){
                    $body[] =[
                        'product' => 'required|digits_between:1,9999999999',
                        'quantity' => 'required|integer'
                        ];
                }
                break;
            case 'DELETE':
                if($id) {
                    $body['products'] = 'required|array';
                }
                break;
        }

        return $body;
    }

    protected function renderBasket($basket, $status = 200)
    {
        if(is_numeric($basket)){
            $basket = Basket::find($basket);
        }

        if (!$basket){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Basket not found.');
        }

        $self1 = '/basket/' . $basket->id;
        $basketProducts = $basket->product;
        $basketEmpty = $basketProducts->isEmpty();

        $nonEmbeded = $basket->toArray();
        unset($nonEmbeded['customer_id']);
        unset($nonEmbeded['product']);

        $hal = new Hal($self1,$nonEmbeded);
        $hal->addLink($this->curiesPrefix . 'createOrder', $self1 . '/order');
        $hal->addLink($this->curiesPrefix . 'emptyBasket', $self1);

        //Add customer as embedded
        $customerEmbeded = new Hal('/customer/' . $basket->customer_id,$basket->customer->toArray());
        $hal->addResource($this->curiesPrefix . 'customer', $customerEmbeded);

        if(!$basketEmpty){
            //Add products
            foreach($basketProducts as $product){
                $productBasket = $product->pivot->toArray();
                unset($productBasket['basket_id']);
                $itemLink = $self1 . '/product/' . $productBasket['product_id'];
                $halBasketProduct = new Hal($itemLink,$productBasket);
                $halBasketProduct->addLink($this->curiesPrefix . 'removeItem', $itemLink);
                $halBasketProduct->addLink($this->curiesPrefix . 'changeQuantity', $itemLink);

                //embed product in Basket item
                $productArray = $product->toArray();
                unset($productArray['created_at']);
                unset($productArray['updated_at']);
                unset($productArray['pivot']);
                $productEmbeded = new Hal('/product/' . $productArray['id'], $productArray);
                $halBasketProduct->addResource($this->curiesPrefix . 'Product', $productEmbeded);

                $hal->addResource($this->curiesPrefix . 'BasketItems', $halBasketProduct);
            }
        }

        return new Response($hal->asJson(), $status, ['Content-Type' => 'application/hal+json']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //search by customer id
        if ($this->request->input('customer')) {
            $customer_id = $this->request->input('customer');
            $validator = Validator::make(['customer' => $customer_id], ['customer' => 'required|integer']);

            if (!$validator->fails()) {
                $basket = Basket::find($customer_id);
                if (!$basket){
                    return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Customer not found.');
                }
                return $this->renderBasket($basket);
            } else {
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Query parameter is not valid.');
            }
        }
        //otherwise get all baskets
        $baskets = Basket::paginate($this->itemsPerPage);
        $extraLinks[] = [
            'rel' => 'find',
            'href' => '/basket{?customer}',
            'attributes' => [
                'templated' => true
            ]
        ];

        return $this->renderCollection($baskets, 'basket', $extraLinks);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $basePath = $this->getRequestedBasePath();

        if ($basePath == 'customer'){
            //check customer exists
            $customer = Customer::find($id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo());
            }

            // get basket or create
            $basket = Basket::firstOrCreate(['customer_id' => $id]);
        } else {
            //get basket
            $basket = Basket::find($id);
        }

        if (!$basket){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Basket not found.');
        }

        return $this->renderBasket($basket);
    }

    /**
     * @param $id
     * @return Response
     */
    public function update($id)
    {
        $basePath = $this->getRequestedBasePath();

        //not accepting any query parameters
        if($this->request->all()){
            return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), 'Resource does Not accept query parameters');
        }

        //get basket by id, or get basket for customer and create it if doesn't exist
        if ($basePath == 'customer'){
            //check customer exists
            $customer = Customer::find($id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Customer not found');
            }

            // get basket or create
            $basket = Basket::firstOrCreate(['customer_id' => $id]);
        } else {
            //get basket
            $basket = Basket::find($id);
        }

        if (!$basket instanceof Basket){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Basket not found');
        }

        $data = $this->getData();
        if($data instanceof ApiProblemResponse){
            return $data;
        }

        $expect = ['product', 'quantity'];
        $reason = 'Invalid data format';

        //validation
        foreach ($data as $obj){
            if(count($obj) != count($expect)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
            }
            foreach ($obj as $field => $value){
                if (!in_array($field, $expect)){
                    return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
                }
                if(!is_numeric($value)){
                    return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
                }
                if($field == 'product'){
                    $product = Product::find($value);
                    if(!$product){
                        $productNotFound = 'Product:' . $value . ' does not found.';
                        return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $productNotFound);
                    }
                }
            }
        }

        $productsInBasket = $basket->product;
        $method = $this->request->method();

        switch ($method) {
            case 'POST':
                //if product is in basket return bad request
                foreach ($data as $product) {
                    if($productsInBasket->contains($product['product'])){
                        $alreadyExist = 'Product: ' . $product['product'] . ' is in basket already.';
                        return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $alreadyExist);
                    }
                }

                //add products to basket
                foreach ($data as $product) {
                    $basket->product()->attach($product['product'], ['quantity' => $product['quantity']]);
                }
                break;
            case 'PATCH':
                foreach ($data as $product) {
                    if (!$productsInBasket->contains($product['product'])) {
                        $notExist = 'Product: ' . $product['product'] . ' is not in basket, you can not modify.';
                        return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $notExist);
                    }
                }
                foreach ($data as $product) {
                    //remove product
                    $basket->product()->detach([$product['product']]);
                    //add product
                    $basket->product()->attach($product['product'], ['quantity' => $product['quantity']]);
                }
                break;
            case 'PUT':
                //remove all products from basket
                $basket->product()->detach();

                //add new products
                foreach ($data as $product) {
                    $basket->product()->attach($product['product'], ['quantity' => $product['quantity']]);
                }
                break;
        }
        $basket->save();


        //update basket total
        if(!basket::updateTotal($basket->id)){
            return ApiProblemResponse::start(500)->setData('Total Not Updated');
        }

        switch ($method) {
            case 'POST':
                return $this->renderBasket($basket->id, 201);
            case 'PATCH':case 'PUT':
                return $this->renderBasket($basket->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $basePath = $this->getRequestedBasePath();

        if ($basePath == 'customer'){
            //check customer exists
            $customer = Customer::find($id);
            if (!$customer){
                return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo());
            }

            // get basket or create
            $basket = Basket::firstOrCreate(['customer_id' => $id]);
        }else {
            $basket = Basket::find($id);
        }

        // if not found return 404
        if (!$basket){
            return ApiProblemResponse::start(404)->setData('', $this->request->getPathInfo(), 'Basket not found.');
        }

        //only remove the given product ids
        if($this->hasPayload()){
            $data = $this->getData();
            if($data instanceof ApiProblemResponse){
                return $data;
            }

            $reason = 'Invalid data format. You can make an options request to "' . $this->request->getPathInfo()
                . '" to check the acceptable data format.';

            //check valid json
            if($data instanceof ApiProblemResponse){
                return $data;
            }

            //check has products key
            if(!array_key_exists('products', $data)){
                return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $reason);
            }

            //validate all product ids given are in basket
            $toDelete = [];
            $productsInBasket = $basket->product;
            foreach ($data as $product) {
                if (!$productsInBasket->contains($product)) {
                    $notExist = 'Product: ' . $product['product'] . ' is not in basket, you can not delete.';
                    return ApiProblemResponse::start(400)->setData('', $this->request->getPathInfo(), $notExist);
                }
                $toDelete[] = $product;
            }

            //delete product ids from basket
            $basket->product()->detach($toDelete);

            //update basket total
            if(!basket::updateTotal($basket->id)){
                return ApiProblemResponse::start(500)->setData('', $this->request->getPathInfo(), 'Basket total not updated');
            }

            //return $this->show($id);

        }else{
            Basket::deleteItems($basket->id);
        }

        return new Response(null, 204);
    }

}
