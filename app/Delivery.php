<?php namespace Awesome;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model {

    protected $table = 'delivery';
    protected $fillable = array('order_id', 'status');
    protected $hidden = ['updated_at'];

    protected static $rules = [
        'order_id' => 'required|digits_between:1,99999999999',
        'status' => 'required|string|in:pending,shipped,delivered,cancelled'
    ];

    public static function validateFields($data)
    {
        $rulesToValidate = [];

        foreach($data as $field => $value){
            $rulesToValidate[$field] = static::$rules[$field];
        }

        $validator = Validator::make($data,$rulesToValidate);
        if ($validator->fails()){
            return false;
        }

        return true;
    }

    public static function getRules()
    {
        return self::$rules;
    }

    public function order()
    {
        return $this->belongsTo('Awesome\Order', 'order_id');
    }

}
