<?php namespace Awesome;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {

    protected $table = 'payment';
    protected $fillable = array('transaction_id', 'order_id');
    protected $hidden = ['updated_at'];

    protected static $rules = [
        'transaction_id' => 'required|alpha_num|size:128',
        'order_id' => 'required|digits_between:1,9999999999'
    ];

    public static function validateFields($data)
    {
        $rulesToValidate = [];

        foreach($data as $field => $value){
            $rulesToValidate[$field] = static::$rules[$field];
        }

        $validator = Validator::make($data,$rulesToValidate);
        if ($validator->fails()){
            return false;
        }

        return true;
    }

    public static function getRules()
    {
        return self::$rules;
    }

    public function order()
    {
        return $this->belongsTo('Awesome\Order');
    }



}
