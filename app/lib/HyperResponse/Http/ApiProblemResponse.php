<?php
namespace Awesome\lib\HyperResponse\Http;

use Crell\ApiProblem\ApiProblem;
use Crell\ApiProblem\RequiredPropertyNotFoundException;
use Illuminate\Http\Response;

/**
 * Error Response represents an HTTP response in API-PROBLEM+JSON format.
 *
 * Note that this class does returned JSON content to be an
 * object. It is however recommended that you do return an object as it
 * protects yourself against XSSI and JSON-JavaScript Hijacking.
 *
 * @see https://www.owasp.org/index.php/OWASP_AJAX_Security_Guidelines#Always_return_JSON_with_an_Object_on_the_outside
 *
 */
class ApiProblemResponse extends Response {

    protected $title;
    protected $type;
    protected $detail;
    protected $instance;
    protected $extensions = [];
    protected $problem;

    /**
     * @param string $title
     * @param string $type
     * @param string $detail
     * @param string $instance
     * @return $this
     */
    public function setData($title = '', $instance = '', $detail = '', $type = '')
    {
        //set title from status text
        if(!$title){
            $this->title = $this->statusText;
        }

        if(!$type){
            $type = 'http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html';
        }

        //create problem object
        if(!$this->problem){
            $this->problem = new ApiProblem($this->title, $type);
        }

        //set problem detail
        if($detail){$this->problem->setDetail($detail);}

        if($instance){
            $this->problem->setInstance($instance);
            if($this->getStatusCode() == 400){
                $this->setExtensions(['information'=> 'Make an OPTIONS request to ' . $instance . ' for information on methods that accept a Body and accepted data formats.' ]);
            }
        }

        //set problem status
        $this->problem->setStatus($this->getStatusCode());

        return $this->update();
    }

    public function setDetail($detail)
    {
        $this->detail = $detail;
        $this->problem->setDetail($detail);
        return $this->update();
    }

    public function setExtensions(array $data = [])
    {
        foreach($data as $key => $value){
            if(is_string($key)){
                $this->extensions[$key] = $value;
                $this->problem[$key] = $value;
            }
        }

        return $this->update();
    }

    protected function update()
    {
        $this->setContent($this->problem->asJson());
        return $this;
    }

    public static function start($status = 400)
    {
        $problem = new ApiProblemResponse('', $status, ['Content-Type' => 'application/problem+json']);
        return $problem->setData();
    }
}