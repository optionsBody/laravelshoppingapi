<?php namespace Awesome;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Product extends Model {

    protected $table = 'product';
    protected $fillable = array('name', 'price');
    protected static $rules = [
        'name' => 'required|max:100',
        'price' => 'required|digits_between:0000.00,9999.99'
    ];

    // public function order()
    // {
    //     return $this->belongsToMany('App\Order', 'order_product', 'product_id', 'id');
    // }

     public function basket()
     {
         return $this->belongsToMany('Awesome\Basket', 'basket_product', 'product_id', 'basket_id');
     }

    /**
     * @param $data
     * @return Validator
     * @return bool
     */
    public static function validateFields($data)
    {
        $rulesToValidate = [];

        foreach($data as $field => $value){
            $rulesToValidate[$field] = static::$rules[$field];
        }

        $validator = Validator::make($data,$rulesToValidate);
        if ($validator->fails()){
            return $validator;
        }

        return true;
    }

    public static function getRules()
    {
        return self::$rules;
    }

    public static function search($input, $paginate = null)
    {
        $query = DB::table('product');
        if(isset($input['page'])){
            unset($input['page']);
        }
        foreach($input as $field => $value){
            $query->where($field, 'LIKE', '%'.$value.'%');
        }

        if($paginate){
            return $query->paginate($paginate)->appends($input);//->get();
        }
        return $query->get();
    }
}
