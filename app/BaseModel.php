<?php namespace Awesome;
/**
 * Created by PhpStorm.
 * User: adamalyan
 * Date: 24/05/15
 * Time: 21:19
 */

use Illuminate\Database\Eloquent\Model;
use Validator;

class BaseModel extends Model {
    public static $rules = [];

    public function __construct()
    {
        parent::__construct();
    }

    public static function validateFields($data)
    {
        $rulesToValidate = [];

        foreach($data as $field => $value){
            $rulesToValidate[$field] = static::$rules[$field];
        }

        $validator = Validator::make($data,$rulesToValidate);
        if ($validator->fails()){
            return false;
        }

        return true;
    }

    public static function getRules()
    {
        return self::$rules;
    }
}