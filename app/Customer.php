<?php namespace Awesome;
/**
 * Created by PhpStorm.
 * User: adamalyan
 * Date: 07/05/15
 * Time: 20:29
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Customer extends Model {
    //The lower-case, plural name of the class will be used as the table name unless another name is explicitly specified.
    //Specifying table name
    protected $table = 'customer';
    protected $fillable = array('first_name', 'last_name', 'email', 'address');
    //protected $hidden = ['created_at'];
    public static $rules = [
        'first_name' => 'required|max:100',
        'last_name' => 'required|max:100',
        'email' => 'required|unique:customer|email',
        'address' => 'required'
    ];

    public function basket()
    {
        return $this->hasOne('Awesome\Basket');
    }

    public function payment()
    {
        return $this->hasManyThrough('Awesome\Payment', 'Awesome\Order', 'customer_id', 'order_id');
    }

    public function delivery()
    {
        return $this->hasManyThrough('Awesome\Delivery', 'Awesome\Order', 'customer_id', 'order_id');
    }

    public function order()
    {
        return $this->hasMany('Awesome\Order');
    }

    /**
     * @param $data
     * @return Validator
     * @return bool
     */
    public static function validateFields($data)
    {
        $rulesToValidate = [];

        foreach($data as $field => $value){
            $rulesToValidate[$field] = static::$rules[$field];
        }

        $validator = Validator::make($data,$rulesToValidate);
        if ($validator->fails()){
            return $validator;
        }

        return true;
    }

    public static function getRules()
    {
        return self::$rules;
    }

    public static function search($input, $paginate = null)
    {
        $query = DB::table('customer');
        if(isset($input['page'])){
            unset($input['page']);
        }
        foreach($input as $field => $value){
            $query->where($field, 'LIKE', '%'.$value.'%');
        }

        if($paginate){
            return $query->paginate($paginate)->appends($input);//->get();
        }
        return $query->get();
    }

}